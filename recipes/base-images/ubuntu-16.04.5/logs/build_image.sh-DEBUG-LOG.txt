Base Release Image [Missing] ubuntu-base-16.04.5-base-amd64.tar.gz
Attempting to download http://cdimage.ubuntu.com/ubuntu-base/releases/16.04.5/release/ubuntu-base-16.04.5-base-amd64.tar.gz
+ wget http://cdimage.ubuntu.com/ubuntu-base/releases/16.04.5/release/ubuntu-base-16.04.5-base-amd64.tar.gz -O depends/ubuntu-base-16.04.5-base-amd64.tar.gz
--2019-06-22 13:23:26--  http://cdimage.ubuntu.com/ubuntu-base/releases/16.04.5/release/ubuntu-base-16.04.5-base-amd64.tar.gz
Resolving cdimage.ubuntu.com (cdimage.ubuntu.com)... 2001:67c:1360:8001::27, 2001:67c:1360:8001::28, 2001:67c:1360:8001::1d, ...
Connecting to cdimage.ubuntu.com (cdimage.ubuntu.com)|2001:67c:1360:8001::27|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 42431881 (40M) [application/x-gzip]
Saving to: ‘depends/ubuntu-base-16.04.5-base-amd64.tar.gz’

depends/ubuntu-base-16.04.5-base 100%[========================================================>]  40.47M   254KB/s    in 2m 14s  

2019-06-22 13:25:41 (309 KB/s) - ‘depends/ubuntu-base-16.04.5-base-amd64.tar.gz’ saved [42431881/42431881]

+ '[' 1 -ne 0 ']'
+ set +x
Base Relese Image Download [Good] ubuntu-base-16.04.5-base-amd64.tar.gz
+ docker import depends/ubuntu-base-16.04.5-base-amd64.tar.gz ubuntu:16.04.5
sha256:1efa3cdb03d6333d4e49eeaffaa509992cbc37759ab63fbb4584597fc9231598
+ docker image ls -a
REPOSITORY               TAG                 IMAGE ID            CREATED                  SIZE
ubuntu                   16.04.5             1efa3cdb03d6        Less than a second ago   115MB
+ docker system df
TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              11                  4                   123.4GB             87.26GB (70%)
Containers          4                   0                   743.1MB             743.1MB (100%)
Local Volumes       0                   0                   0B                  0B
Build Cache         0                   0                   0B                  0B
+ '[' 1 -ne 0 ']'
+ set +x
